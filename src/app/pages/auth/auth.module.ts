import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';

import {AuthComponent} from './auth.component';
import {AuthRoutingModule} from './auth-routing.module';
import {MaterialModule} from '../../material.module';

@NgModule({
  declarations: [AuthComponent],
  imports: [
    AuthRoutingModule,
    MaterialModule,
    FormsModule,
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  exports: []
})

export class AuthModule {}
