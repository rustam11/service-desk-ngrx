import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {NgForm} from '@angular/forms';

import * as fromApp from '../../reducers';
import * as AuthActions from './store/auth.actions';

import {UiService} from '../../services/ui.service';
import {ReplaySubject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
  isLoginMode = true;
  isLoading = false;
  error: string = null;

  unsubscribe$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private store: Store<fromApp.AppState>,
    private uiService: UiService
  ) { }

  ngOnInit() {
    this.store.select('auth')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(authState => {
          this.isLoading = authState.loading;
          this.error = authState.authError;
          if (this.error) {
            this.uiService.showShackbar(this.error, null, 3000)
          }
        }
      );
  }

  onSwitchMode(form: NgForm) {
    this.isLoginMode = !this.isLoginMode;
    form.reset();
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;

    if (this.isLoginMode) {
      this.store.dispatch(new AuthActions.LoginStart({email, password}));
    } else {
      this.store.dispatch(new AuthActions.SignupStart({email, password}));
    }
    form.reset();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
