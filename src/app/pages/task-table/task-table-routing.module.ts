import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TaskTableComponent} from './task-table.component';
import {AuthGuard} from '../auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: TaskTableComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TaskTableRoutingModule {

}
