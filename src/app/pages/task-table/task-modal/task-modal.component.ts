import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import {map, takeUntil} from 'rxjs/operators';
import {ReplaySubject} from 'rxjs';
import * as fromApp from '../../../reducers';
import * as TaskActions from '../store/task.actions';
import {TaskModel} from '../task.model';
import {UiService} from '../../../services/ui.service';
import {environment} from '../../../../environments/environment';


@Component({
  selector: 'app-new-task',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.scss']
})
export class TaskModalComponent implements OnInit, OnDestroy {
  isEditMode = false;
  taskForm: FormGroup;
  tableData: TaskModel;
  unsubscribe$: ReplaySubject<boolean> = new ReplaySubject(1);
  userId: string = null;
  isAdmin = false;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: TaskModalComponent,
    public dialogRef: MatDialogRef<TaskModalComponent>,
    private uiService: UiService,
    private store: Store<fromApp.AppState>
  ) {
    if (data && data.data) {
      this.isEditMode = data.data.isEditMode;
      this.tableData = data.data.tableData;
    }
  }

  ngOnInit() {
    this.store.select('auth').pipe(
      map(authState => {
        return authState.user;
      }))
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        this.userId = user.id;
        if (user.id === environment.adminId) {
          this.isAdmin = true;
        }
      });
    this.setupForm();
    if (this.isEditMode) {
      this.taskForm.patchValue(this.tableData);
    }
  }

  disableControls() {
    return !this.isAdmin && this.isEditMode;
  }

  setupForm() {
    this.taskForm = new FormGroup({
      title: new FormControl(
        '',
        {
          updateOn: 'change',
          validators: [Validators.required, Validators.minLength(3), Validators.maxLength(60)]
        }
      ),
      executor: new FormControl(
        '',
        {
          updateOn: 'change'
        }
      ),
      description: new FormControl(
        '',
        {
          updateOn: 'change'
        }
      ),
      priority: new FormControl(
        'med',
        {
          updateOn: 'change',
          validators: [Validators.required]
        }
      ),
      status: new FormControl(
        'notActive',
        {
          updateOn: 'change',
          validators: [Validators.required]
        }
      ),
    });
  }

  onStatusChange() {
    if (this.taskForm.value.status !== 'notActive') {
      this.taskForm.controls.executor.setValidators([Validators.required, Validators.minLength(5), Validators.maxLength(50)]);
    } else {
      this.taskForm.controls.executor.setValidators([]);
    }
    this.taskForm.controls.executor.updateValueAndValidity();
  }

  onSubmitForm() {
    if (!this.isEditMode) {
      const formValues: TaskModel = {
        ...this.taskForm.value,
        userId: this.userId,
        date: new Date().toJSON()
      };
      this.saveData(formValues, () => {
          this.uiService.showShackbar('Task was created successfully', null, 3000);
          this.onCloseModal();
        }
      );
    } else {
      const formValues: TaskModel = {
        ...this.taskForm.value,
        date: new Date().toJSON()
      };
      this.updateTask(formValues, () => {
        this.uiService.showShackbar('Task was updated successfully', null, 3000);
        this.onCloseModal();
      });
    }
  }

  saveData(value, complete = () => {
  }) {
    this.store.dispatch(new TaskActions.AddTask(value));
    complete();
  }

  updateTask(value: TaskModel, complete = () => {
  }) {
    const newTask = {...this.tableData, ...value};
    this.store.dispatch(new TaskActions.UpdateTask({index: this.tableData.taskId, newTask}));
    complete();
  }

  onCloseModal() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
