import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  progress: number;
}

@Component({
  selector: 'app-stop-training',
  template:
    `
      <h1 mat-dialog-title>Are you sure you want to delete this task?</h1>
      <mat-dialog-actions>
        <button mat-button [mat-dialog-close]="true">
          Yes
        </button>
        <button mat-button [mat-dialog-close]="false">
          No
        </button>
      </mat-dialog-actions>
    `
})

export class DeleteTaskComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
}
