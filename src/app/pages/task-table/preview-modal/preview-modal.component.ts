import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';

import {map, takeUntil} from 'rxjs/operators';
import {ReplaySubject} from 'rxjs';
import {TaskModel} from '../task.model';
import * as fromApp from '../../../reducers';
import {DeleteTaskComponent} from './delete-task';
import * as TasksAtions from '../store/task.actions';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-preview-modal',
  templateUrl: './preview-modal.component.html',
  styleUrls: ['./preview-modal.component.scss']
})
export class PreviewModalComponent implements OnInit, OnDestroy {
  task: TaskModel = null;
  isAdmin = false;
  unsubscribe$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PreviewModalComponent,
    public dialogRef: MatDialogRef<PreviewModalComponent>,
    private dialog: MatDialog,
    private store: Store<fromApp.AppState>
  ) {
    if (data && data.data) {
      this.task = data.data.task;
    }
  }

  ngOnInit() {

    this.store.select('auth').pipe(
      map(authState => {
        return authState.user;
      }))
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (user.id === environment.adminId) {
          this.isAdmin = true;
        }
      });
  }

  onCloseModal() {
    this.dialogRef.close();
  }

  onEditTask() {
    this.dialogRef.close(this.task);
  }

  onDeleteTask() {
    const dialogRef = this.dialog.open(DeleteTaskComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onCloseModal();
        this.store.dispatch(new TasksAtions.DeleteTask(this.task.taskId));
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
