export interface TaskModel {
  title: string;
  executor: string;
  description: string;
  priority: string;
  status: string;
  taskId?: string;
  userId: string;
  date: Date;
}
