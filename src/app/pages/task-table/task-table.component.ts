import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {MatDialog, MatDialogConfig, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Store} from '@ngrx/store';
import {BreakpointObserver} from '@angular/cdk/layout';

import {map, takeUntil} from 'rxjs/operators';
import {ReplaySubject, Subject} from 'rxjs';

import * as fromApp from '../../reducers';
import * as TaskActions from './store/task.actions';
import {TaskModel} from './task.model';
import {TaskModalComponent} from './task-modal/task-modal.component';
import {PreviewModalComponent} from './preview-modal/preview-modal.component';

@Component({
  selector: 'app-task-table',
  templateUrl: './task-table.component.html',
  styleUrls: ['./task-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class TaskTableComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  dataSource = new MatTableDataSource<TaskModel>(null);
  displayedColumns = ['title', 'executor', 'priority', 'status'];
  tasks: TaskModel[] = null;

  unsubscribe$: ReplaySubject<boolean> = new ReplaySubject(1);
  loadingError$ = new Subject<boolean>();


  constructor(
    public dialog: MatDialog,
    private breakpointObserver: BreakpointObserver,
    private store: Store<fromApp.AppState>,
    private cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit() {

    this.store.dispatch(new TaskActions.FetchTasks());
    this.store.select('tasks')
      .pipe(
        map(tasksState => tasksState.tasks),
        takeUntil(this.unsubscribe$)
      ).subscribe(tasks => {
      this.dataSource.data = tasks;
      this.cdr.detectChanges();
    }, error => {
      this.loadingError$.next(true);
      this.cdr.detectChanges();
    });
  }

  onCreateNewTask() {
    const newTaskModal = this.dialog.open(TaskModalComponent, {
      width: '80vw',
      panelClass: this.breakpointObserver.isMatched('(max-width: 599px)') ? 'fullscreen-overlay' : '',
      disableClose: false
    });
    newTaskModal.afterClosed().subscribe(() => this.cdr.detectChanges());
  }

  trackByTaskId(index: number, item: TaskModel) {
    if (!item) {
      return null;
    }
    return item.taskId;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilterData(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onOpenPreview(tableData: TaskModel) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      task: tableData
    };
    const previewModal = this.dialog.open(PreviewModalComponent, {
      width: '80vw',
      panelClass: this.breakpointObserver.isMatched('(max-width: 599px)') ? 'fullscreen-overlay' : '',
      disableClose: false,
      data: dialogConfig
    });
    previewModal.afterClosed().subscribe((modalRes: TaskModel) => {
      if (modalRes) {
        this.cdr.detectChanges();
        this.onEditTask(modalRes);
      }
    });
  }

  onEditTask(task: TaskModel) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      tableData: task,
      isEditMode: true
    };
    this.dialog.open(TaskModalComponent, {
      width: '80vw',
      panelClass: this.breakpointObserver.isMatched('(max-width: 599px)') ? 'fullscreen-overlay' : '',
      disableClose: false,
      data: dialogConfig
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
