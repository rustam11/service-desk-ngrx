import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import {TaskTableComponent} from './task-table.component';
import {MaterialModule} from '../../material.module';
import {TaskTableRoutingModule} from './task-table-routing.module';
import {TaskModalComponent} from './task-modal/task-modal.component';
import {PipeModule} from '../../pipe/pipe.module';
import { PreviewModalComponent } from './preview-modal/preview-modal.component';
import {DeleteTaskComponent} from './preview-modal/delete-task';

@NgModule({
  declarations: [TaskTableComponent, TaskModalComponent, PreviewModalComponent, DeleteTaskComponent],
  imports: [MaterialModule, FlexLayoutModule, TaskTableRoutingModule, ReactiveFormsModule, CommonModule, PipeModule],
  exports: [],
  entryComponents: [TaskModalComponent, PreviewModalComponent, DeleteTaskComponent]
})

export class TaskTableModule {

}
