import {Action} from '@ngrx/store';

import {TaskModel} from '../task.model';

export const SET_TASKS = '[Task] Set Tasks';
export const ADD_TASK = '[Task] Add Task';
export const ADD_TASK_SUCCESS = '[Task] Add Task Successful';
export const FETCH_TASKS = '[Task] Fetch Tasks';
export const UPDATE_TASK = '[Task] Update Task';
export const DELETE_TASK = '[Task] Delete Task';

export class SetTasks implements Action {
  readonly type = SET_TASKS;

  constructor(public payload: TaskModel[]) {
  }
}

export class AddTaskSuccessful implements Action {
  readonly type = ADD_TASK_SUCCESS;

  constructor(public payload: TaskModel) {
  }
}

export class AddTask implements Action {
  readonly type = ADD_TASK;

  constructor(public payload: TaskModel) {
  }
}

export class FetchTasks implements Action {

  readonly type = FETCH_TASKS;
}

export class UpdateTask implements Action {
  readonly type = UPDATE_TASK;

  constructor(public payload: { index: string, newTask: TaskModel }) {
  }

}

export class DeleteTask implements Action {
  readonly type = DELETE_TASK;

  constructor(public payload: string) {
  }

}

export type TaskActions =
  | SetTasks
  | AddTask
  | AddTaskSuccessful
  | FetchTasks
  | UpdateTask
  | DeleteTask;
