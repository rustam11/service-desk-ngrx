import {TaskModel} from '../task.model';
import * as TaskActions from './task.actions';

export interface State {
  tasks: TaskModel[];
}

const initialState: State = {
  tasks: []
};

export function tasksReducer(
  state = initialState,
  action: TaskActions.TaskActions
) {
  switch (action.type) {

    case TaskActions.SET_TASKS: {
      return {
        ...state,
        tasks: [...action.payload]
      };
    }

    case TaskActions.FETCH_TASKS: {
      return state;
    }
    case TaskActions.ADD_TASK_SUCCESS:
      return {
        ...state,
        tasks: [...state.tasks, action.payload]
      };
    case TaskActions.UPDATE_TASK:
      const updatedTask: TaskModel = {
        ...state.tasks.filter(task => task.taskId === action.payload.index),
        ...action.payload.newTask
      };
      const updatedTasks = [...state.tasks];
      updatedTasks[updatedTasks.findIndex(el => el.taskId === action.payload.index)] = updatedTask;
      return {
        ...state,
        tasks: updatedTasks
      };

    case TaskActions.DELETE_TASK:
      return {
        ...state,
        tasks: state.tasks.filter(task => task.taskId !== action.payload)
      };
    default:
      return state;
  }
}
