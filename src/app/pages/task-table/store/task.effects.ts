import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {HttpClient} from '@angular/common/http';

import {map, switchMap} from 'rxjs/operators';
import * as TaskActions from './task.actions';
import {TaskModel} from '../task.model';


@Injectable()

export class TaskEffects {
  @Effect()
  addTask = this.actions$.pipe(
    ofType(TaskActions.ADD_TASK),
    switchMap((taskAction: TaskActions.AddTask) => {
      return this.http.post<{ name: string }>(
        `https://service-desk-8a61e.firebaseio.com/tasks.json`, taskAction.payload
      )
        .pipe(
          map((resData: { name: string }) => {
            const task = {...taskAction.payload, taskId: resData.name};
            return new TaskActions.AddTaskSuccessful(task);
          })
        );
    })
  );

  @Effect()
  fetchTasks = this.actions$.pipe(
    ofType(TaskActions.FETCH_TASKS),
    switchMap(() => {
      return this.http.get<{ [key: string]: TaskModel }>(`https://service-desk-8a61e.firebaseio.com/tasks.json`);
    }),
    map((resData: { [key: string]: TaskModel }) => {
      const tasks: TaskModel[] = [];
      for (const key in resData) {
        if (resData.hasOwnProperty(key)) {
          const task: TaskModel = {
            title: resData[key].title,
            executor: resData[key].executor,
            description: resData[key].description,
            priority: resData[key].priority,
            status: resData[key].status,
            date: new Date(resData[key].date),
            userId: resData[key].userId,
            taskId: key,
          };
          tasks.push(task);
        }
      }
      return tasks;
    }),
    map(tasks => {
      return new TaskActions.SetTasks(tasks);
    })
  );

  @Effect({dispatch: false})
  deleteTask = this.actions$.pipe(
    ofType(TaskActions.DELETE_TASK),
    switchMap((actionData: TaskActions.DeleteTask) => {
      return this.http.delete<null>(`https://service-desk-8a61e.firebaseio.com/tasks/${actionData.payload}.json`);
    })
  );

  @Effect({dispatch: false})
  updateTask = this.actions$.pipe(
    ofType(TaskActions.UPDATE_TASK),
    switchMap((actionData: TaskActions.UpdateTask) => {
      return this.http.put(`https://service-desk-8a61e.firebaseio.com/tasks/${actionData.payload.index}.json`, actionData.payload.newTask);
    })
  );

  constructor(private actions$: Actions,
              private http: HttpClient) {
  }
}
