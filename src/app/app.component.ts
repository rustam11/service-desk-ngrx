import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import * as fromApp from './reducers';
import * as AuthActions from './pages/auth/store/auth.actions';
import {User} from './pages/auth/user.modal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  user$: Observable<User>;

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit(): void {
    this.store.dispatch(new AuthActions.AutoLogin());
    this.user$ = this.store.select('auth').pipe(
      map(authState => {
        return authState.user;
      }));
  }


  onLogout() {
    this.store.dispatch(new AuthActions.Logout());
  }
}
