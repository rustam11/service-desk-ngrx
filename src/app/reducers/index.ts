import {
  ActionReducerMap
} from '@ngrx/store';

import * as fromTasks from '../pages/task-table/store/task.reducer';
import * as fromAuth from '../pages/auth/store/auth.reducer';


export interface AppState {
  tasks: fromTasks.State;
  auth: fromAuth.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  tasks: fromTasks.tasksReducer,
  auth: fromAuth.authReducer
};
