import {NgModule} from '@angular/core';

import {OutputFormValuePipe} from './output-form-value.pipe';

@NgModule({
  declarations: [OutputFormValuePipe],
  exports: [OutputFormValuePipe]
})

export class PipeModule {}
