import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'outputFormValue'
})
export class OutputFormValuePipe implements PipeTransform {

  transform(value: string) {
    switch (value) {
      case 'notActive':
        return 'Not Active';
      case 'planned':
        return 'Planned';
      case 'done':
        return 'Done';
      case 'checked':
        return 'Checked';
      case 'closed':
        return 'Closed';
      case 'low':
        return 'Low';
      case 'med':
        return 'Medium';
      case 'high':
        return 'High';
      default:
        return value;
    }
  }
}
